﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.IO;
using System.Collections.ObjectModel;
using Microsoft.Office.Interop.Word;
using System.Text.RegularExpressions;

namespace TextReplace
{
    class ApplicationModel : INotifyPropertyChanged
    {
        private string folder;
        private string mask;
        private string excludeMask;
        private string find;
        private string replace;
        private SearchOption subdirectories;

        public ApplicationModel()
        {
            folder = "";
            mask = "";
            excludeMask = "";
            find = "";
            replace = "";
            subdirectories = SearchOption.TopDirectoryOnly;
        }

        public string Folder
        {
            get { return folder; }
            set
            {
                folder = value;
                OnPropertyChanged("Folder");
            }
        }

        public string Mask
        {
            get { return mask; }
            set
            {
                mask = value;
                OnPropertyChanged("Mask");
            }
        }

        public string ExcludeMask
        {
            get { return excludeMask; }
            set
            {
                excludeMask = value;
                OnPropertyChanged("ExcludeMask");
            }
        }

        public string Find
        {
            get { return find; }
            set
            {
                find = value;
                OnPropertyChanged("Find");
            }
        }

        public string Replace
        {
            get { return replace; }
            set
            {
                replace = value;
                OnPropertyChanged("Replace");
            }
        }

        public bool SubDirectories
        {
            get
            {
                if (subdirectories == SearchOption.AllDirectories) return true;
                else return false;
            }
            set
            {
                if (value) subdirectories = SearchOption.AllDirectories;
                else subdirectories = SearchOption.TopDirectoryOnly;

                OnPropertyChanged("SubDirectories");
            }
        }

        public bool checkMask(string fileName)
        {
            string[] exts = excludeMask.Split('|', ',', ';');
            string pattern = string.Empty;
            foreach (string ext in exts)
            {
                pattern += @"^";
                foreach (char symbol in ext)
                    switch (symbol)
                    {
                        case '.': pattern += @"\."; break;
                        case '?': pattern += @"."; break;
                        case '*': pattern += @".*"; break;
                        default: pattern += symbol; break;
                    }
                pattern += @"$|";
            }
            if (pattern.Length == 0) return false;
            pattern = pattern.Remove(pattern.Length - 1);
            Regex mask = new Regex(pattern, RegexOptions.IgnoreCase);
            return mask.IsMatch(System.IO.Path.GetFileName(fileName));
        }

        /*
                private void GetAllFiles(ObservableCollection<string> files, object sender)
                {

                    Stack<string> dirs = new Stack<string>();

                    if (!Directory.Exists(folder))
                    {
                        throw new ArgumentException();
                    }
                    dirs.Push(folder);

                    while (dirs.Count > 0)
                    {
                        string currentDir = dirs.Pop();
                        string[] subDirs;
                        try
                        {
                            subDirs = Directory.GetDirectories(currentDir);
                        }
                        catch (UnauthorizedAccessException e)
                        {
                            Console.WriteLine(e.Message);
                            continue;
                        }
                        catch (DirectoryNotFoundException e)
                        {
                            Console.WriteLine(e.Message);
                            continue;
                        }

                        string[] tempfiles = null;
                        try
                        {
                            tempfiles = Directory.GetFiles(currentDir);
                        }
                        catch (UnauthorizedAccessException e)
                        {
                            Console.WriteLine(e.Message);
                            continue;
                        }
                        catch (DirectoryNotFoundException e)
                        {
                            Console.WriteLine(e.Message);
                            continue;
                        }

                        foreach (string file in tempfiles)
                        {
                            try
                            {
                                files.Add(file);
                            }
                            catch (FileNotFoundException e)
                            {
                                Console.WriteLine(e.Message);
                                continue;
                            }
                        }

                        foreach (string str in subDirs)
                            dirs.Push(str);
                    } 
                    //files.Clear();
                    BackgroundWorker worker = sender as BackgroundWorker;
                    string[] file = Directory.GetFiles(folder, (Mask.Equals("") ? "*" : Mask), SearchOption.AllDirectories);
                    int progressPercentage = Convert.ToInt32(((double)1 / file.Count()) * 100);

                    foreach (string temp in file)
                    {
                        files.Add(temp);
                        worker.ReportProgress(progressPercentage, "Searching files");
                        System.Threading.Thread.Sleep(50);
                    } 
                }*/

        public void FileContainsText(object sender, DoWorkEventArgs e)
        {
            string text;
            BackgroundWorker worker = sender as BackgroundWorker;

            int count = 0; 
            List<string> result = new List<string>();
            string[] allfiles = Directory.GetFiles(folder, (Mask.Equals("") ? "*" : Mask), (SearchOption)subdirectories);

            List<string> files = new List<string>();
            foreach (string temp in allfiles)
                if (!checkMask(temp)) files.Add(temp);

            double percent = ((double)1 / files.Count()) * 100;
            double progressPercentage = 0; 

            foreach (string temp in files)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                    
                string extention = Path.GetExtension(temp); 

                if(extention.Equals(".doc") || extention.Equals(".docx"))
                {
                    Application app = new Application();
                    app.Documents.Open(temp);
                    Document doc = app.ActiveDocument;

                    text = doc.Content.Text;

                    if (text.Contains(find))
                        result.Add(temp);

                    doc.Save();
                    doc.Close();
                    app.Quit();
                }
                else
                {
                    if (extention.Equals(".txt") || extention.Equals(".html") || extention.Equals(".css"))
                    {
                        text = File.ReadAllText(temp);
                        if (text.Contains(find))
                            result.Add(temp);
                    }
                }
                count++;
                progressPercentage += percent;
                worker.ReportProgress((int)Math.Round(progressPercentage), $"Searching files contains text ({count} / {files.Count()})");
            }

            e.Result = result;
            worker.DoWork -= FileContainsText;
        }

        public void ReplaceFiles(object sender, DoWorkEventArgs e)
        {
            string text;
            BackgroundWorker worker = sender as BackgroundWorker;

            FileContainsText(sender, e);

            List<string> files = e.Result as List<string>;
            List<string> result = new List<string>();

            int count = 0;
            double percent = ((double)1 / files.Count()) * 100;
            double progressPercentage = 0;

            foreach (string temp in files)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                string extention = Path.GetExtension(temp);
                string newText;

                if (extention.Equals(".doc") || extention.Equals(".docx"))
                {
                    Application app = new Application();
                    app.Documents.Open(temp);
                    Document doc = app.ActiveDocument;

                    text = doc.Content.Text;
                    newText = text.Replace(find, replace);
                    doc.Content.Text = newText;

                    doc.Save();
                    doc.Close();
                    app.Quit();
                }
                else
                {
                    if (extention.Equals(".txt") || extention.Equals(".html") || extention.Equals(".css"))
                    {
                        text = File.ReadAllText(temp);
                        newText = text.Replace(find, replace);
                        File.WriteAllText(temp, newText);
                    }   
                }

                count++;
                progressPercentage += percent;
                worker.ReportProgress((int)Math.Round(progressPercentage), $"Replacing ({count} / {files.Count()})");
            }

            worker.DoWork -= ReplaceFiles;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}